<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap5\Breadcrumbs;
use yii\bootstrap5\Html;
use yii\bootstrap5\Nav;
use yii\bootstrap5\NavBar;
use yii\bootstrap5\ActiveForm;

AppAsset::register($this);

$this->registerCsrfMetaTags();
$this->registerMetaTag(['charset' => Yii::$app->charset], 'charset');
$this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, shrink-to-fit=no']);
$this->registerMetaTag(['name' => 'description', 'content' => $this->params['meta_description'] ?? '']);
$this->registerMetaTag(['name' => 'keywords', 'content' => $this->params['meta_keywords'] ?? '']);
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => Yii::getAlias('@web/favicon.ico')]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">

<head>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="d-flex flex-column h-100">
    <?php $this->beginBody() ?>

    <header id="header">
        <?php
        NavBar::begin([
            'brandLabel' => Yii::$app->name,
            'brandUrl' => Yii::$app->homeUrl,
            'options' => ['class' => 'navbar-expand-md navbar-dark bg-dark fixed-top']
        ]);
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav'],
            'items' => [
                ['label' => 'Home', 'url' => ['/site/index']],
                ['label' => 'About', 'url' => ['/site/about']],
                ['label' => 'Contact', 'url' => ['/site/contact']],
                Yii::$app->user->isGuest
                    ? ['label' => 'Login', 'url' => ['/site/login']]
                    : '<li class="nav-item">'
                    . Html::beginForm(['/site/logout'])
                    . Html::submitButton(
                        'Logout (' . Yii::$app->user->identity->username . ')',
                        ['class' => 'nav-link btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>'
            ]
        ]);
        NavBar::end();
        ?>
    </header>

    <main id="main" class="flex-shrink-0" role="main">
        <div class="container">
            <?php $form = ActiveForm::begin([
                'action' => ['userapicall'],
                'fieldConfig' => [
                    'template' => "{label}\n{input}\n{error}",
                    'labelOptions' => ['class' => 'col-lg-1 col-form-label mr-lg-3'],
                    'inputOptions' => ['class' => 'col-lg-3 form-control'],
                    'errorOptions' => ['class' => 'col-lg-7 invalid-feedback'],
                ],
            ]); ?>



            <center>
                <b>Maklumat API & Carian</b>
                <div class="px-5">
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Scope</label>
                        <select class="form-select" aria-label="Default select example" name="scope">
                            <option selected>Sila Pilih</option>
                            <option value="HRMACCPROFILEIC">HRMACCPROFILEIC</option>
                            <option value="HRMACCPROFILETRN">HRMACCPROFILETRN - Query Maklumat Kursus Pukal Failed[The query has timed out.]</option>
                            <option value="HRMACCPROFILELV">HRMACCPROFILELV</option>
                            <option value="HRMACCPROFILEPR">HRMACCPROFILEPR</option>
                            <option value="HRMACCPROFILEADDR">HRMACCPROFILEADDR</option>
                            <option value="HRMACCPROFILEFMY">HRMACCPROFILEFMY</option>
                            <option value="HRMACCPROFILELG">HRMACCPROFILELG</option>
                            <option value="HRMACCPROFILEHISSERV">HRMACCPROFILEHISSERV</option>
                            <option value="HRMACCPROFILEDIS">HRMACCPROFILEDIS</option>
                            <option value="HRMACCPROFILEMEDHIS">HRMACCPROFILEMEDHIS</option>
                            <option value="HRMACCPROFILESFMED">HRMACCPROFILESFMED</option>
                            <option value="HRMACCPROFILEEDU">HRMACCPROFILEEDU</option>
                            <option value="HRMACCPROFILEPS">HRMACCPROFILEPS</option>
                            <option value="HRMACCPROFILEAWD">HRMACCPROFILEAWD</option>
                            <option value="HRMACCPROFILESERV">HRMACCPROFILESERV - Query Maklumat Perkhidmatan Pukal Failed[The query has timed out.]</option>
                            <option value="HRMACCPROFILEASSET">HRMACCPROFILEASSET</option>
                            <option value="HRMACCPROFILELNPT">HRMACCPROFILELNPT - Parameter name does not exist - MappingResult15</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">No KP</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="NoKP" value="841231016314"> <!-- value="841231016314" -->
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Bulan dan Tahun</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="Tarikh"> <!-- value="062023"-->
                    </div>
                </div>
                <br>
                <br>
                <b>Maklumat Credential</b>
                <div class="px-5">
                    <div class="mb-3">
                        <label for="exampleInputEmail2" class="form-label">Username</label>
                        <input type="text" class="form-control" id="exampleInputEmail2" name="username"> <!--value="gmaca2024" -->
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" name="password">
                    </div>
                </div>

                <div class="form-group">
                    <div>
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    </div>
                </div>
            </center>

            <?php ActiveForm::end(); ?>
        </div>
    </main>

    <footer id="footer" class="mt-auto py-3 bg-light">
        <div class="container">
            <div class="row text-muted">
                <div class="col-md-6 text-center text-md-start">&copy; My Company <?= date('Y') ?></div>
                <div class="col-md-6 text-center text-md-end"><?= Yii::powered() ?></div>
            </div>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>