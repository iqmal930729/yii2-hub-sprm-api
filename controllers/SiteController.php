<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */

    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionUserapicall()
    {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $scope = $_POST['scope'];
        $NoKP = $_POST['NoKP'];
        $Tarikh = $_POST['Tarikh'];

        $postfields = array('scope' => $scope);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('username:' . $username, 'password:' . $password));
        curl_setopt($ch, CURLOPT_URL, 'http://10.25.164.92/api/checkCredential');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        // Edit: prior variable $postFields should be $postfields;
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // On dev server only!
        $result = curl_exec($ch);
        $jsondecode = json_decode($result);
        // print_r($jsondecode);
        // exit;
        if (isset($jsondecode->token)) {
            $params = [
                'NoKP' => $NoKP,
                'Tarikh' => $Tarikh
            ];

            // Append parameters to URL
            $url = 'http://10.25.164.92/api/apiCode';

            // Bearer token
            $token = $jsondecode->token;

            // Initialize cURL
            $ch = curl_init();

            // Set the URL and options
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Authorization: Bearer ' . $token,
                'Content-Type: application/json'
            ]);

            //disable ssl
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));


            // Execute the request
            $response = curl_exec($ch);

            // Check for errors
            if ($response === false) {
                $error = curl_error($ch);
                curl_close($ch);
                die('cURL Error: ' . $error);
            }

            // Close cURL
            curl_close($ch);

            // Decode and print the response
            $responseData = json_decode($response, true);
            echo "<pre>";
            print_r($responseData['data']['MaklumatKursus']);
            echo "</pre>";
            // die;
            // foreach ($responseData['data']['MaklumatKursus'] as $mk) {
            //     echo "<pre>";
            //     print_r($mk['NoKP'] . ",<br>");
            //     echo "</pre>";
            // }
            die;
            // return $this->redirect(['/site/view_api']);
        }
    }

    // public function actionView_api()
    // {
    //     $data = $_POST['data'];
    //     print_r($data);
    //     exit;
    // }
}
